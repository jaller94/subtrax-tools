import {
    generatePuzzle,
} from './generate.js';

describe('generatePuzzle', () => {
  describe('with seed 1', () => {
    test('have a property solution', () => {
      expect(generatePuzzle(1)).toHaveProperty('solution');
    });
    test('matches snapshot', () => {
      expect(generatePuzzle(1)).toMatchSnapshot();
    });
  });
});
