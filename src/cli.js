'use strict';
import fsPromises from 'node:fs/promises';

import {
    generatePuzzle,
    svgCard,
} from './generate.js';

async function main() {
    const puzzle = generatePuzzle();
    console.log(puzzle);
    await fsPromises.writeFile('puzzle.json', JSON.stringify(puzzle, undefined, 4), 'utf8');
    await fsPromises.writeFile('puzzle.svg', svgCard(puzzle), 'utf8');
    await fsPromises.writeFile('solution.svg', svgCard(puzzle, true), 'utf8');
}

main();