'use strict';

function mulberry32(a) {
    return function () {
        var t = a += 0x6D2B79F5;
        t = Math.imul(t ^ t >>> 15, t | 1);
        t ^= t + Math.imul(t ^ t >>> 7, t | 61);
        return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }
}

/**
 * @readonly
 * @enum {number}
 */
const PINS = {
    None: 0,
    Orange: 1,
    Blue: 2,
};

const HOPS = [
    [[3, 7]], // A (0)
    [[4, 9], [3, 5]], // B (1)
    [[3, 4], [5, 10]], // C (2)
    [[4, 6], [5, 8], [7, 12]], // D (3)
    [[3, 2], [7, 10], [9, 14]], // E (4)
    [[3, 1], [7, 9], [10, 15]], // F (5)
    [[4, 3], [9, 12]], // G (6)
    [[3, 0], [9, 11], [10, 13]], // H (7)
    [[5, 3], [10, 12]], // I (8)
    [[4, 1], [7, 5], [12, 15]], // J (9)
    [[5, 2], [7, 4], [12, 14]], // K (10)
    [[9, 7]], // L (11)
    [[7, 3], [9, 6], [10, 8]], // M (12)
    [[10, 7]], // N (13)
    [[9, 4]], // O (14)
    [[10, 5]], // P (15)
];

function randomInt(rand, min, max) {
    return Math.floor(rand() * (max - min)) + min;
}

function randomItem(rand, arr) {
    return arr[randomInt(rand, 0, arr.length - 1)];
}

/**
 * @param {[number, number][][]} hops
 * @param {PINS[]} pins
 * @param {number} position
 */
function getPossibleHops(hops, pins, position) {
    return hops[position].filter(hop => pins[hop[0]] === PINS.None && pins[hop[1]] === PINS.None);
}

export function generatePuzzle(seed) {
    seed = seed ?? randomInt(Math.random, 0, 1000);
    const rand = mulberry32(seed);
    const pins = Array(HOPS.length).fill(PINS.None);
    const solution = [];
    const start = randomInt(rand, 0, HOPS.length - 1);
    pins[start] = PINS.Orange;

    let fails = 0;
    while (fails < 6) {
        let possiblePositions = pins.reduce((positions, pin, index) => pin === PINS.None ? positions : [...positions, index], []);
        let currentPosition = randomItem(rand, possiblePositions);
        if (currentPosition !== undefined) {
            while (getPossibleHops(HOPS, pins, currentPosition).length > 0) {
                const possibleHops = getPossibleHops(HOPS, pins, currentPosition);
                const chosenHop = randomItem(rand, possibleHops);
                pins[chosenHop[0]] = PINS.Blue;
                pins[chosenHop[1]] = pins[currentPosition];
                pins[currentPosition] = PINS.None;
                if (solution.length > 0 && solution[0][0] === currentPosition) {
                    solution[0].unshift(chosenHop[1]);
                } else {
                    solution.unshift([chosenHop[1], currentPosition]);
                }
                currentPosition = chosenHop[1];
            }
        }
        fails++;
    }
    return {
        pins,
        seed,
        solution,
    };
}

export function svgCard(puzzle, isSolution = false) {
    const pinColors = {
        [PINS.None]: 'white',
        [PINS.Orange]: 'orange',
        [PINS.Blue]: 'blue',
    };
    const X = i => [3, 2, 4, 3, 2, 4, 1, 3, 5, 2, 4, 1, 3, 5, 2, 4][i] * 80;
    const Y = i => [0, 1, 1, 2, 3, 3, 4, 4, 4, 5, 5, 6, 6, 6, 7, 7][i] * 40 + 40;
    const names = 'ABCDEFGHIJKLMNOP';
    let xml = `<svg viewBox="0 0 480 480" xmlns="http://www.w3.org/2000/svg" stroke="grey" fill="white">\n`;
    // Lines from top left to bottom right
    xml += `    <line x1="${X(0)}" y1="${Y(0)}" x2="${X(2)}" y2="${Y(2)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(1)}" y1="${Y(1)}" x2="${X(8)}" y2="${Y(8)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(4)}" y1="${Y(4)}" x2="${X(13)}" y2="${Y(13)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(6)}" y1="${Y(6)}" x2="${X(15)}" y2="${Y(15)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(11)}" y1="${Y(11)}" x2="${X(14)}" y2="${Y(14)}" stroke="gray" />\n`;
    // Lines from top right to bottom left
    xml += `    <line x1="${X(0)}" y1="${Y(0)}" x2="${X(1)}" y2="${Y(1)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(2)}" y1="${Y(2)}" x2="${X(6)}" y2="${Y(6)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(5)}" y1="${Y(5)}" x2="${X(11)}" y2="${Y(11)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(8)}" y1="${Y(8)}" x2="${X(14)}" y2="${Y(14)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(13)}" y1="${Y(13)}" x2="${X(15)}" y2="${Y(15)}" stroke="gray" />\n`;

    xml += `    <line x1="${X(6)}" y1="${Y(6)}" x2="${X(11)}" y2="${Y(11)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(1)}" y1="${Y(1)}" x2="${X(14)}" y2="${Y(14)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(0)}" y1="${Y(0)}" x2="${X(12)}" y2="${Y(12)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(2)}" y1="${Y(2)}" x2="${X(15)}" y2="${Y(15)}" stroke="gray" />\n`;
    xml += `    <line x1="${X(8)}" y1="${Y(8)}" x2="${X(13)}" y2="${Y(13)}" stroke="gray" />\n`;
    for (let i = 0; i < HOPS.length; i++) {
        const color = pinColors[puzzle.pins[i]];
        xml += `    <circle cx="${X(i)}" cy="${Y(i)}" r="20" fill="${color}" />\n`;
        if (isSolution) {
            xml += `    <text x="${X(i)}" y="${Y(i)}" dominant-baseline="middle" text-anchor="middle" stroke="none" fill="black" style="font-size: 20px">${names[i]}</text>\n`;
        }
    };
    if (isSolution && puzzle.solution.length > 0) {
        xml += `    <text x="50%" y="400" dominant-baseline="middle" text-anchor="middle" stroke="none" fill="black" style="font-size: 30px">${puzzle.solution.map(positions => positions.map(position => names[position]).join('➝')).join(' / ')}</text>\n`;
        xml += `    <text x="50%" y="440" dominant-baseline="middle" text-anchor="middle" stroke="none" fill="gray" style="font-size: 24px">${puzzle.solution.length} MOVES</text>\n`;
    }
    xml += `</svg>`;
    return xml;
}
