# Subtrax Tools

This is a collection of scripts to generate, validate and solve puzzles of Subtrax.

Subtrax is a trademark of [ThinkFun](https://www.thinkfun.com/) and was invented by Nob Yoshigahara. This fan project is not affiliated with ThinkFun nor Nob Yoshigahara. No assets with their copyright have been used.

## Generate a puzzle and its solution
```sh
npm install
node src/cli.js
```

### Puzzle
![](./docs/puzzle-example.png)

### Solution
![](./docs/solution-example.png)

## Validate a puzzle

_TODO_

## Solve a puzzle

_TODO_

## License

Copyright (C) 2022  The Subtrax Tools authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
